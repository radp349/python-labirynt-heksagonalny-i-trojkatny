# Readme
Ten projekt w Pythonie generuje i wyświetla labirynt na podstawie losowego algorytmu Kruskala. Algorytm ten może prowadzić do utworzenia ścian labiryntu niepołączonych do głównej ściany, co może skutkować mniej skomplikowanymi strukturami labiryntów, ale umożliwia tworzenie pętli. 

## Zależności
- numpy
- matplotlib.pyplot

## Używanie skryptu
Aby wygenerować i wyświetlić labirynt, wystarczy uruchomić skrypt. Rozmiar labiryntu można dostosować wywołując funkcję `create_maze()` z parametrami `width` i `height`, które domyślnie są ustawione na 5.

```python
create_maze(7, 7)
```

Powoduje to wygenerowanie i wyświetlenie labiryntu o wymiarach 7x7. Bezpośrednie uruchomienie skryptu spowoduje wygenerowanie labiryntu o domyślnych rozmiarach 5x5.

## Opis funkcji
- `set_boarders(y1, y2, x, width, height)`: Ta funkcja ustawia ściany wokół generowanego labiryntu. Argumenty to macierze reprezentujące ściany oraz szerokość i wysokość labiryntu.

- `maze_hex(width=5, height=5, complexity=0.85)`: Ta funkcja generuje labirynt używając algorytmu Kruskala. Argumenty to szerokość, wysokość i złożoność labiryntu. Złożoność jest mnożnikiem określającym liczbę ścian w labiryncie.

- `create_maze(width=5, height=5)`: Ta funkcja generuje i wyświetla labirynt. Argumenty to szerokość i wysokość labiryntu.

Znaczenie różnych typów ścian oraz szczegółowe wyjaśnienie działania funkcji `maze_hex()` oraz `create_maze()` jest dostępne bezpośrednio w komentarzach kodu.
