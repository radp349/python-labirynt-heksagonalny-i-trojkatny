import numpy as np
from numpy.random import randint as rand
import matplotlib.pyplot as plt
# Program tworzy labirynt losowanym algorytmem Kruskala,
# wykorzystanie tego algorytmu powoduje utworzenie innego typu labiryntu w którym możliwe są ściany nie połączone do
# ściany głównej, labirynty mogą wydawać się mniej skomplikowane ale dają możliwość zapętleń, co trzeba uwzględnić
# w algorytmie wyszukiwania.
# Program znajduje też ścieżkę od lewgo dolnego rogu do prawego górnego
# Niektóre wersje labiryntu powodują bardzo długi czas szukania ścieżki,
# wtedy lepiej przeładować program żeby nie tracić czasu, ponieważ tego typu labirynty nie są częste,
# a czas oczekiwania na wynik zazwyczaj jest krótki.
# Wielkość labiryntu może być modyfikowana w wywołaniu funkcji create_maze(),
# algorytm będzie zwracał dalej poprawne wyniki, bazowo labirynt jest wielkość 5 na 5 co oznacza pięć hexów zewnętrznych
# na pięć hexów zewnętrznych
p3 = 1.73205080757


def set_boarders(x1, x2, y, width, height):  # funkcja ustawiająca ściany wokół labiryntu
    x1[:, 0] = 1
    x2[:, height -1] = 1
    y[0, :] = 1
    y[width * 2 -1, :] = 1

    return x1, x2, y



def maze_hex(width=5, height=5, complexity=0.6):  # funkcja generująca labirynt losowanym algorytmem Kruskala
    shape = (height, width)
    points = np.zeros((2 * width + 1, (3 * height) + (height - 1)), dtype=bool)
    complexity = int(complexity * (125 * (shape[0] + shape[1])))
    # tutaj ustawiane są ściany na podstawie których tworzony jest labirynt
    # ściany są podzielone na dwa rodzaje wertykalnych i jeden horyzontalny
    connections_x1 = np.zeros((width , height), dtype=int)
    connections_x2 = np.zeros((width - 1, height), dtype=int)
    connections_y = np.zeros((width * 2, height * 2 - 1), dtype=int)
    connections_x1, connections_x2, connections_y = set_boarders(connections_x1, connections_x2,
                                                                 connections_y, width, height)
    for d in range(complexity):  # losowanie ścian labiryntu
        # tylko do jednego punktu może przylegać ściana aby stworzyć nową ścianę między dwoma punktami
        a = rand(0, 8)  # losowanie typu ściany do wypełnienia
        if a == 0:
            x, y = rand(0, width), rand(0, height)
            if y > 0:
                if x > 0:
                    if ((connections_x1[x -1, y ] == 0) and ((connections_y[2*x, 2*y ] == 0) or (connections_y[2*x +1, 2*y ] == 0))
                        and ((connections_y[2*x, 2*y -1 ] == 0) or (connections_y[2*x +1, 2*y -1] == 0))):
                        connections_x1[x, y] = 1
                elif x<width-1:
                    if ((connections_x1[x +1, y ] == 0) and ((connections_y[2*x, 2*y ] == 0) or (connections_y[2*x +1, 2*y ] == 0))
                        and ((connections_y[2*x, 2*y -1 ] == 0) or (connections_y[2*x +1, 2*y -1] == 0))):
                        connections_x1[x, y] = 1
    #     elif a == 1:
    #         x, y, direction = rand(0, width), rand(0, height - 1), rand(0, 2)
    #         if (connections_x[x * 2, 1 + y * 2 + direction] == 0) & (connections_x[x * 2 + 1, y * 2 + direction] == 0):
    #             connections_x2[x, y] = 1
    #     if a > 1:
    #         x, y, direction = rand(0, width * 2), rand(0, height * 2), rand(0, 2)
    #         if ((x % 2 == 1) & (y % 2 == 1)) | ((x % 2 == 0) & (y % 2 == 0)):
    #             if (direction == 1) & (x < width * 2 - 1):
    #                 if (x % 2 == 1) & (connections_y1[int((x + 1) / 2), int((y - 1) / 2)] == 0) & (
    #                         connections_x[x + 1, y] == 0):
    #                     connections_y[x, y] = 1

    #             if (direction == 0) & (x > 1) & (y < height * 2 - 1):
    #                 if (x % 2 == 1) & (connections_y2[int((x - 1) / 2), int((y - 1) / 2)] == 0) \
    #                         & (connections_x[x - 1, y] == 0):
    #                     connections_y[x, y] = 1

    #         if ((x % 2 == 0) & (y % 2 == 1)) | ((x % 2 == 1) & (y % 2 == 0)):
    #             if (direction == 1) & (x < width * 2 - 1) & (y < height * 2 - 1):
    #                 if (x % 2 == 1) & (connections_y2[int((x - 1) / 2), int((y - 1) / 2)] == 0) & (
    #                         connections_x[x + 1, y] == 0):
    #                     connections_y[x, y] = 1
    #             if (direction == 0) & (x > 1) & (y < height * 2 - 1):
    #                 if (x % 2 == 1) & (connections_y1[int((x + 1) / 2), int((y - 1) / 2)] == 0) \
    #                         & (connections_x[x - 1, y] == 0):
    #                     connections_y[x, y] = 1
    
    connections_y[0, 0] = 0
    connections_y[2*width-1, 2*height-2] = 0
    return points, connections_x1, connections_x2, connections_y


# funkcja wyświetlająca labirynt
def create_maze(width=5, height=5):
    plt.figure(figsize=(9, 9))

    plt.subplot(111)
    points, cons_x1, cons_x2, cons_y = maze_hex(width, height)
    plt.axis([-10, len(points[0, :]) * 2 +10, -10, len(points[0, :]) * 2 +10])

    #wyświetlane wygenerowanego labiryntu
    for X in range(len(points[:, 0])):
        for Y in range(len(points[0, :])):
            if X % 2 == 0:
                if Y % 4 == 0:
                    plt.plot(2*X, ((p3 ) * Y), 'r.')
            if X % 2 == 1:
                if Y % 4 == 2:
                    plt.plot(2*X, ((p3 ) * Y), 'r.')

    for i in range(len(cons_x1[:, 0])):
        for j in range(len(cons_x1[0, :])):
            if cons_x1[i, j] == 1:
                plt.plot([4 * i, 4 * i + 4], [4*p3 * j, 4*p3 * j ], 'orange')

    for i in range(len(cons_x2[:, 0])):
        for j in range(len(cons_x2[0, :])):
            if cons_x2[i, j] == 1:
                plt.plot([4 * i + 2, 4 * i + 6], [4 * p3 * j + 2*p3, 4 * p3 * j + 2*p3], 'g')

    for i in range(len(cons_y[:, 0])):
        for j in range(len(cons_y[0, :])):
            if cons_y[i, j] == 1:
                if ((i % 2 == 0) & (j % 2 == 0)) | ((i % 2 == 1) & (j % 2 == 1)):
                    plt.plot([2 * i, 2 * i + 2], [2 *p3 *j, 2*p3*j + 2*p3], 'purple')
                if ((i % 2 == 0) & (j % 2 == 1)) | ((i % 2 == 1) & (j % 2 == 0)):
                    plt.plot([2 * i + 2, 2 * i ], [2 *p3 *j, 2*p3*j + 2*p3], 'y')


    plt.show()


create_maze(5, 5)
